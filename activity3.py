class Camper():

	

	def __init__(self, name, batch, course_type):

		self.name = name
		self.batch = batch
		self.course_type = course_type



	# methods
	def info(self):
		print(f'Currently enrolled in : {self.course_type}')

	def career_track(self):
		print(f'My name is {self.name} from {self.batch}')

new_course = Camper("A. Einstein", "batch 6", "Physics XXI")


print(f"Camper Name: {new_course.name}")
print(f"Camper Batch: {new_course.batch}")
print(f"Camper Course: {new_course.course_type}")
new_course.career_track()
new_course.info()